import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import StarBorder from '@material-ui/icons/StarBorder';
import Popover from '@material-ui/core/Popover';
import './ListItem.scss';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(3),
  },
  menuActive: {
    boxShadow: 'inset 0.1875rem 0 0 #007bff',
    backgroundColor: ' #fbfbfb',
    color: '#007bff',
  },
  onMinWidthMode: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'middle',
  },
  subMenu: {
    borderTop: '1px solid #e1e5eb',
    // padding: '.75rem 1.75rem',
    borderBottom: '1px solid #f0f2f5',
  },
  menu: {
    padding: '.9375rem 1.5625rem',
  },
}));

export default function NestedList(props) {
  let { isMinWidth } = props;
  const classes = useStyles();
  const [active, setActive] = React.useState([]);
  const [anchorEl, setAnchorEl] = React.useState(null);

  function handleClick(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }
  const items = [1, 2, 3, 4, 5];
  function toggleActive(index, event) {
    if (isMinWidth) {
      setAnchorEl(event.currentTarget);
      setActive(active === index ? null : index);
    } else {
      setActive(active === index ? null : index);
    }
  }
  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;
  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      // subheader={
      //   <ListSubheader component="div" id="nested-list-subheader">
      //     Nested List Items
      //   </ListSubheader>
      // }
      className={classes.root}
    >
      {items.map((item, index) => {
        return (
          <div key={index} className='menu-hover'>
            <ListItem
              onClick={e => toggleActive(index, e)}
              button
              className={`${classes.menu} ${active === index && classes.menuActive} ${isMinWidth &&
                classes.onMinWidthMode}`}
            >
              <ListItemIcon className={`${isMinWidth && classes.onMinWidthMode} curent-color`}>
                <SendIcon className="curent-color" />
              </ListItemIcon>
              {!isMinWidth && <ListItemText primary="Sent mail" />}
              {!isMinWidth && (active === index ? <ExpandLess /> : <ExpandMore />)}
            </ListItem>
            {!isMinWidth && (
              <Collapse in={active === index} timeout="auto" unmountOnExit className='collapse-submenu'>
                <List component="div" disablePadding>
                  <ListItem button className={`${classes.nested} ${classes.subMenu} submenu`}>
                    <ListItemText primary="Starred" />
                  </ListItem>
                  <ListItem button className={`${classes.nested} ${classes.subMenu} submenu`}>
                    <ListItemText primary="Starred" />
                  </ListItem>
                  <ListItem button className={`${classes.nested} ${classes.subMenu} submenu`}>
                    <ListItemText primary="Starred" />
                  </ListItem>
                  <ListItem button className={`${classes.nested} ${classes.subMenu} submenu`}>
                    <ListItemText primary="Starred" />
                  </ListItem>
                  <ListItem button className={`${classes.nested} ${classes.subMenu} submenu`}>
                    <ListItemText primary="Starred" />
                  </ListItem>
                </List>
              </Collapse>
            )}

            {isMinWidth && (
              <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'left',
                }}
                style={{ boxShadow: 'none' }}
              >
                <List component="div" disablePadding style={{ minWidth: 200, boxShadow: 'none' }}>
                  <ListItem button className={`${classes.nested} ${classes.subMenu}`}>
                    <ListItemText primary="Starred" />
                  </ListItem>
                  <ListItem button className={`${classes.nested} ${classes.subMenu}`}>
                    <ListItemText primary="Starred" />
                  </ListItem>
                  <ListItem button className={`${classes.nested} ${classes.subMenu}`}>
                    <ListItemText primary="Starred" />
                  </ListItem>
                  <ListItem button className={`${classes.nested} ${classes.subMenu}`}>
                    <ListItemText primary="Starred" />
                  </ListItem>
                  <ListItem button className={`${classes.nested} ${classes.subMenu}`}>
                    <ListItemText primary="Starred" />
                  </ListItem>
                </List>
              </Popover>
            )}
          </div>
        );
      })}

      {/* <ListItem button>
        <ListItemIcon>
          <DraftsIcon />
        </ListItemIcon>
        <ListItemText primary="Drafts" />
      </ListItem>
      <ListItem button onClick={handleClick}>
        <ListItemIcon>
          <InboxIcon />
        </ListItemIcon>
        <ListItemText primary="Inbox" />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem> */}
    </List>
  );
}
