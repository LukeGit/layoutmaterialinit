import React, { Component } from "react";
import { Layout, Icon, Badge, Popover, Menu, Dropdown, Button } from 'antd';
import './ToolBar.scss';
import Avatar from 'react-avatar';


const { Header } = Layout;


const menu = (
  <Menu style={{ minWidth: 150 }}>
    <Menu.Item style={{ display: 'flex', alignItems: 'center' }}>
      <Icon type="setting" />  <span>Profile </span>
    </Menu.Item>
    <Menu.Item style={{ display: 'flex', alignItems: 'center' }}>
      <Icon type="mail" />  <span>Message </span>
    </Menu.Item>
    <Menu.Item style={{ display: 'flex', alignItems: 'center' }}>
      <Icon type="logout" /><span>Logout  </span>
    </Menu.Item>
  </Menu>
);

class HeaderContent extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    return (
      <div className="app-header" >
        <div className="app-header__search-box">
          <Icon type="search" />
          <input placeholder="Search..." />
        </div>
        <div className="app-header__user-info">
          <Avatar name="Wim Mostmans" size="35" round={true} />
          <p>Wim Mostmans</p>
          {/* <Badge dot={true} status="success"  >
          
          </Badge> */}
          <Icon style={{marginLeft:5}} type="bell" />
        </div>
        <Dropdown overlay={menu} placement="bottomRight" trigger={['click']}>
          <div className="setting" >
            <Icon type="menu" />
          </div>
        </Dropdown>
        {/* <div className="setting" >
          <Popover placement="bottomRight"
            content={
              <div>
                <div className="setting-item">
                  <p>Setting </p><Icon type="setting" />
                </div>
                <div className="setting-item">
                  <p>Message </p> <Icon type="mail" />
                </div>
                <div className="setting-item">
                  <p>Logout  </p><Icon type="logout" />
                </div>
              </div>
            } trigger="click">
            <Icon type="menu" />
          </Popover>
        </div> */}

      </div>
    );
  }
}

export default HeaderContent

